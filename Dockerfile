FROM node:carbon
# Create app directory

ARG REACT_DIRECTORY

WORKDIR /usr/src/docker-react-sample
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)

COPY ./${REACT_DIRECTORY}/package.json ./
RUN npm install

#To bundle your app’s source code inside the Docker image, use the COPY instruction:COPY . .
#Your app binds to port 3000 so you’ll use the EXPOSE instruction to have it mapped by the docker daemon:

COPY run.sh .
RUN chmod +x run.sh

EXPOSE 3000
CMD ["/usr/src/docker-react-sample/run.sh"]